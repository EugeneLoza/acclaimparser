(* Parses animation data from ACCLAIM animation file *)
unit AnimationParser;

{$mode objfpc}{$H+}

interface

uses
  AcclaimSpecifications;

procedure ParseAcclaimAnimation(const AnimationFileName: String);
implementation
uses
  CastleVectors,
  SysUtils;

procedure ParseAcclaimAnimation(const AnimationFileName: String);
var
  { Animation file }
  F: Text;
  { Full string, Trimmed string read from the file
    Cropped is cropped each time next word is read from the file }
  S, T, Cropped: String;

  function FirstSymbol(const aSymbol: String): Boolean;
  begin
    Result := (Copy(S, 1, 1) = aSymbol);
  end;
  function SkipLine: Boolean;
  begin
    Result := FirstSymbol('#') or FirstSymbol(':');
  end;
  function isNumber: Boolean;
  begin
    if FirstSymbol('0') or
       FirstSymbol('1') or
       FirstSymbol('2') or
       FirstSymbol('3') or
       FirstSymbol('4') or
       FirstSymbol('5') or
       FirstSymbol('6') or
       FirstSymbol('7') or
       FirstSymbol('8') or
       FirstSymbol('9') then
         Result := true
    else
      Result := false;
  end;
  procedure ReadLine;
  begin
    ReadLn(F, S);
    T := Trim(S);
    if SkipLine then
      ReadLine;
    Cropped := T;
    //bug: may encounter EOF here
  end;
  function NextWord: String;
  var
    I: Integer;
    S1: String;
  begin
    Result := '';
    I := 1;
    repeat
      S1 := Copy(Cropped, I, 1);;
      if S1 <> ' ' then
        Result += S1;
      Inc(I);
    until ((S1 = ' ') and (Length(Result) > 0)) or (I > Length(Cropped));
    Result := LowerCase(Result);
    Cropped := Copy(Cropped, I, Length(Cropped)); //no need to +1
  end;
  procedure ReadAnimationSection;
  var
    BoneName: String;
    I: Integer;
    CurrentBone: TBone;
    CurrentAnimation: TAnimationArray;
    RotationVector: TVector3;
    function isSectionEnd: Boolean;
    begin
      Result := isNumber or EOF(F);
    end;
  begin
    repeat
      ReadLine;
      if not isSectionEnd then
      begin
        BoneName := NextWord;
        CurrentBone := BoneDictionary.Items[BoneName];
        //for I := 0 to Pred(Length(BoneDictionary.Items[BoneName].BoneData.DegreesOfFreedom)) do //error while linking :D
        CurrentAnimation := AnimationDictionary.Items[BoneName];
        SetLength(CurrentAnimation, Length(CurrentAnimation) + 1);
        for I := 0 to Pred(Length(CurrentBone.BoneData.DegreesOfFreedom)) do
        begin
          case CurrentBone.BoneData.DegreesOfFreedom[I] of
            'rx': RotationVector.Data[0] := StrToFloat(NextWord);
            'ry': RotationVector.Data[1] := StrToFloat(NextWord);
            'rz': RotationVector.Data[2] := StrToFloat(NextWord);
            //else ignore
          end;
        end;
        CurrentAnimation[Pred(Length(CurrentAnimation))] := RotationVector;
        AnimationDictionary.Items[BoneName] := CurrentAnimation;
      end;
    until isSectionEnd;
  end;
begin
  WriteLn('Reading file ' + AnimationFileName);
  AssignFile(F, AnimationFileName);
  Reset(F);

  AnimationDictionary := TAnimationDictionary.Create({[doOwnsValues]});
  for S in BoneDictionary.Keys do
    AnimationDictionary.Add(S, []);

  ReadLine;
  repeat
    if isNumber then
      ReadAnimationSection;
  until EOF(F);
  CloseFile(F);
end;

finalization
  FreeAndNil(AnimationDictionary);

end.

