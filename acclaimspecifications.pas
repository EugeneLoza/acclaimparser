(* This unit describes ACCLAIM skeleton objects,
   such as bones, hierarchy and animation streams }
unit AcclaimSpecifications;

{$mode objfpc}{$H+}

interface

uses
  Generics.Collections,
  CastleVectors;

type
  { A simple dynamic array of string to store string data }
  TStringArray = array of String;
  { A simple dynamic array of TVector2 to store limits (max-min) values }
  TLimitsArray = array of TVector2;

type
  { Root is a special bone, all other bones descend from it }
  TRootData = record
    { in which order do the root's fields go in animation file }
    Order: TStringArray;
    {}
    //Axis: String; assuming XYZ
    { global position of root (i.e. of object) }
    Position: TVector3;
    { global orientation of root }
    Orientation: TVector3;
  end;

type
  { Data on each bone }
  TBoneData = record
    { name of the bone, IMPORTANT: used for ALL operations }
    BoneName: String;
    { Initial direction of the bone }
    Direction: TVector3;
    { Length of the bone }
    Length: Single;
    {}
    Axis: TVector3;
    { in which order do bones' field go in animation file }
    DegreesOfFreedom: TStringArray;
    { limits to DegreesOfFreedom values (for procedural animations) }
    Limits: TLimitsArray;
  end;

type
  { Dictionary of TBoneData, arranged by TBoneData.BoneName }
  TBoneDataDictionary = specialize TDictionary<String, TBoneData>;

type
  { forward declaration }
  TBoneList = class;
  { this is a hierarchy bone }
  TBone = class(TObject)
  public
    { current parameters of this bone }
    BoneData: TBoneData;
    { Children of this bone in hierarchy }
    Children: TBoneList;
    { draw bones hierarchy in console }
    procedure Visualize(const aPrefix: String);
  public
    constructor Create; //override;
    destructor Destroy; override;
  end;
  { arrangement of bones }
  TBoneList = class(specialize TObjectList<TBone>);
  TBoneDictionary = specialize TObjectDictionary<String, TBone>;
  { special bone - root, uses RootData instead of BoneData }
  TRoot = class(TBone)
  public
    RootData: TRootData;
  end;

type
  { a simple dynamic array of animation data flow for a single bone }
  TAnimationArray = array of TVector3;

type
  { animations for different bones }
  TAnimationDictionary = specialize TDictionary<String, TAnimationArray>;

var
  { Current Root }
  Root: TRoot;
  { Data on every bone in the skeleton }
  BoneDataDictionary: TBoneDataDictionary;
  { Bone hieararchy }
  BoneDictionary: TBoneDictionary;
  { Skeleton animations }
  AnimationDictionary: TAnimationDictionary;


implementation

procedure TBone.Visualize(const aPrefix: String);
var
  B: TBone;
begin
  WriteLn(aPrefix + BoneData.BoneName);
  for B in Children do
    B.Visualize(aPrefix + '>');
end;

constructor TBone.Create;
begin
  //inherited <------ parent is non-virtual
  Children := TBoneList.Create(true);
end;

destructor TBone.Destroy;
begin
  Children.Free;
  inherited Destroy;
end;

end.

