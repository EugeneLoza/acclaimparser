(* Parses animation data from ACCLAIM skeleton file *)
unit SkeletonParser;

{$mode objfpc}{$H+}

interface

uses
  CastleVectors,
  AcclaimSpecifications;

procedure ParseAcclaimSkeleton(const SkeletonFileName: String);
implementation
uses
  Classes, SysUtils;

procedure ParseAcclaimSkeleton(const SkeletonFileName: String);
var
  { Skeleton file }
  F: Text;
  { Full string, Trimmed string read from the file
    Cropped is cropped each time next word is read from the file }
  S, T, Cropped: String;
  { Root data read from the file (used in different procedures) }
  RootData: TRootData;

  function FirstSymbol(const aSymbol: String): Boolean;
  begin
    Result := (Copy(S, 1, 1) = aSymbol);
  end;
  function CommentLine: Boolean;
  begin
    Result := FirstSymbol('#');
  end;
  function SkipLine: Boolean;
  begin
    Result := (T = '') or CommentLine;
  end;
  function Section: String;
  var
    I: Integer;
    S1: String;
  begin
    Result := '';
    if FirstSymbol(':') then
    begin
      I := 1;
      repeat
        S1 := Copy(T, I, 1);;
        if S1 <> ' ' then
          Result += S1;
        Inc(I);
      until (S1 = ' ') or (I > Length(T));
      Result := LowerCase(Result);
    end;
  end;
  function SymbolSkip(const aSymbol: String): Boolean;
  begin
    Result := (aSymbol = ' ') or (aSymbol = '(') or (aSymbol = ')')
  end;
  function NextWord: String;
  var
    I: Integer;
    S1: String;
  begin
    Result := '';
    I := 1;
    repeat
      S1 := Copy(Cropped, I, 1);;
      if not SymbolSkip(S1) then
        Result += S1;
      Inc(I);
    until ((SymbolSkip(S1)) and (Length(Result) > 0)) or (I > Length(Cropped));
    Result := LowerCase(Result);
    Cropped := Copy(Cropped, I, Length(Cropped)); //no need to +1
  end;
  procedure ReadLine;
  begin
    ReadLn(F, S);
    T := Trim(S);
    if SkipLine then
      ReadLine;
    Cropped := T;
    //bug: may encounter EOF here
  end;
  function SectionEnd: Boolean;
  begin
    Result := (Section <> '') or EOF(F);
  end;
  procedure SkipSection;
  begin
    repeat
      ReadLine;
    until SectionEnd;
  end;
  function ReadSingle: Single;
  begin
    Result := StrToFloat(NextWord);
  end;
  function ReadVector2: TVector2;
  begin
    Result.Data[0] := StrToFloat(NextWord);
    Result.Data[1] := StrToFloat(NextWord);
  end;
  function ReadVector3: TVector3;
  begin
    Result.Data[0] := StrToFloat(NextWord);
    Result.Data[1] := StrToFloat(NextWord);
    Result.Data[2] := StrToFloat(NextWord);
  end;
  function ReadStringArray: TStringArray;
  var
    S1: String;
  begin
    SetLength(Result, 0);
    S1 := NextWord;
    while S1 <> '' do
    begin
      SetLength(Result, Length(Result) + 1);
      Result[Pred(Length(Result))] := S1;
      S1 := NextWord;
    end;
  end;
  procedure ReadRootSection;
  begin
    ReadLine;
    if SectionEnd then
      raise Exception.Create('Unexpected end of Root Section!');
    repeat
      case NextWord of
        'order': RootData.Order := ReadStringArray;
        'axis': {nop}; //RootData.Axis := NextWord; //assuming XYZ
        'position': RootData.Position := ReadVector3;
        'orientation': RootData.Orientation := ReadVector3;
        else
          WriteLn('Unexpected line in Root: ' + S);
      end;
      ReadLine;
    until SectionEnd;
  end;
  function isBegin: Boolean;
  begin
    Result := T = 'begin';
  end;
  function isEnd: Boolean;
  begin
    Result := T = 'end';
  end;
  procedure ReadBoneDataSection;
  var
    Bone: TBoneData;
    I: Integer;
  begin
    BoneDataDictionary := TBoneDataDictionary.Create;

    ReadLine;
    if SectionEnd then
      raise Exception.Create('Unexpected end of BoneData Section!');
    repeat
      if isBegin then
      repeat
        ReadLine;
        case NextWord of
          'id': ;  //we don't need id
          'name': Bone.BoneName := NextWord;
          'direction': Bone.Direction := ReadVector3;
          'length': Bone.Length := ReadSingle;
          'axis': Bone.Axis := ReadVector3;
              //Bone.AxisType = NextWord; //assuming it's XYZ
          'dof': Bone.DegreesOfFreedom := ReadStringArray;
          'limits':
            begin
              SetLength(Bone.Limits, Length(Bone.DegreesOfFreedom));
              for I := 0 to Pred(Length(Bone.Limits)) do
              begin
                if I > 0 then
                  ReadLine;
                Bone.Limits[I] := ReadVector2;
              end;
            end;
          'end': ; //skip it will be processed in `isEnd`.
          else
            WriteLn('Unexpected KeyWord in BoneData: ' + S);
        end;
      until isEnd;
      BoneDataDictionary.Add(Bone.BoneName, Bone);
      ReadLine;
    until SectionEnd;
  end;
  procedure ReadHierarchySection;
  var
    CurrentBoneName, CurrentBoneChild: String;
    Bone: TBone;
  begin
    ReadLine;
    if SectionEnd then
      raise Exception.Create('Unexpected end of Hierarchy Section!');
    Root := TRoot.Create;
    Root.BoneData.BoneName := 'root';
    Root.RootData := RootData;
    BoneDictionary := TBoneDictionary.Create([]);
    BoneDictionary.Add('root', Root);

    repeat
      ReadLine;
      CurrentBoneName := NextWord;
      if CurrentBoneName <> '' then
      begin
        repeat
          CurrentBoneChild := NextWord;
          if CurrentBoneChild <> '' then
          begin
            Bone := TBone.Create;
            Bone.BoneData := BoneDataDictionary.Items[CurrentBoneChild];
            BoneDictionary.Add(CurrentBoneChild, Bone);
            BoneDictionary.Items[CurrentBoneName].Children.Add(Bone);
          end;
        until CurrentBoneChild = '';
      end;
    until SectionEnd;
    Root.Visualize('');
  end;
begin
  WriteLn('Reading file ' + SkeletonFileName);
  AssignFile(F, SkeletonFileName);
  Reset(F);
  ReadLine;
  repeat
    case Section of
      ':version':       SkipSection;
      ':name':          SkipSection;
      ':units':         SkipSection; //we assume degrees for angles
      ':documentation': SkipSection;
      ':root':          ReadRootSection;
      ':bonedata':      ReadBoneDataSection;
      ':hierarchy':     ReadHierarchySection;
    else
      WriteLn('Unknown section ', S);
    end;
  until EOF(F);
  CloseFile(F);
end;

finalization
  FreeAndNil(BoneDataDictionary);
  FreeAndNil(BoneDictionary);
  FreeAndNil(Root);
end.

