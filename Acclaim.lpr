program Acclaim;
uses
  SysUtils,
  CastleWindow,
  SkeletonParser, AnimationParser;

{
  SEE DOCUMENTATION HERE
  http://research.cs.wisc.edu/graphics/Courses/cs-838-1999/Jeff/ASF-AMC.html

  MOCAP can be downloaded here
  http://mocap.cs.cmu.edu/ (Public Domain)
  http://kitchen.cs.cmu.edu/main.php ("This data is free to use for any purpose.")
  http://resources.mpi-inf.mpg.de/HDM05/ (CC-BY-SA)
}

var
  Window: TCastleWindow;

begin
  Window := TCastleWindow.Create(Application);
  ParseAcclaimSkeleton('01.asf');
  ParseAcclaimAnimation('01_01.amc');
  Window.OpenAndRun;
end.

